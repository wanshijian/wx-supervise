package com.ztfgroup.supervise.intercept;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @desc: 拦截器类
 * 攔截所有請求，排除靜態資源文件
 */
@Configuration
@EnableWebMvc
public class WebAppConfig implements WebMvcConfigurer {

    @Bean
    public ZtWxInterceptor ztWxInterceptor() {
        return new ZtWxInterceptor();
    }

    /**
     * 配置拦截器
     *
     * @param registry
     * @author lance
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        InterceptorRegistration interceptor = registry.addInterceptor(ztWxInterceptor());
//        interceptor.addPathPatterns("/**").excludePathPatterns("/static/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");
    }
}