package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.InstructFeedback;
import com.ztfgroup.supervise.entity.vo.InstructFeedbackVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-03-14
 */
public interface InstructFeedbackMapper extends BaseMapper<InstructFeedback> {


    /**
     * 新增指示反馈
     * @param feedback
     * @return
     */
    Integer addInstructFeeback(InstructFeedback feedback);


    /**
     * 查询指示反馈
     * @param instructId
     * @return
     */
    List<InstructFeedbackVO> findInstructFeedBack(@Param("instructId") String instructId);

}
