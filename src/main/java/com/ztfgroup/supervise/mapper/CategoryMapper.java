package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.vo.CategoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 项目板块表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface CategoryMapper extends BaseMapper<Category> {

    /**
     * 以当前用户USER_ID 查询其所能见的项目板块
     * @param userId 用户USER_ID
     * @return 项目板块
     */
    List<Category> findTaskCategory(@Param("userId") String userId);

    /**
     * 查询全部项目板块
     * @return 项目板块
     */
    List<Category> findAllCategory();

    /**
     * 查询任务信息及最新反馈
     * @param selfUserId
     * @return
     */
    List<CategoryVO> findTaskAndNewestFeedback(@Param(value = "selfUserId") String selfUserId, @Param(value = "deptId") String deptId, @Param(value = "otherUserId") String otherUserId);


    /**
     * 查询任务看板页面信息
     * @param selfUserId
     * @param categoryId
     * @return
     */
    List<CategoryVO> findCategoryDeptList(@Param(value = "userId") String selfUserId, @Param(value = "categoryId") String categoryId);


    /**
     * 查询任务追踪列表
     * @param selfUserId
     * @return
     */
    List<CategoryVO> findTaskTrackList(@Param(value = "selfUserId") String selfUserId);

}
