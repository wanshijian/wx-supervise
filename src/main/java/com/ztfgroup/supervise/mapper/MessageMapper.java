package com.ztfgroup.supervise.mapper;

import com.ztfgroup.supervise.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-03-07
 */
@Repository
public interface MessageMapper extends BaseMapper<Message> {


    /**
     * 新增推送消息，记录表中
     * @param message
     * @return
     */
    Integer addMessage(Message message);

}
