package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.UserTask;
import com.ztfgroup.supervise.entity.vo.RoleTaskVO;
import com.ztfgroup.supervise.entity.vo.UserTaskVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 人员任务表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface UserTaskMapper extends BaseMapper<UserTask> {


    /**
     * 按USER_ID 和TASK_ID查询与任务有关的其他用户
     * @param userId
     * @param taskId
     * @return
     */
    List<UserTaskVO> findUserInfoByTaskId(@Param("userId") String userId, @Param("taskId") String taskId);


    /**
     * 按USER_ID 和TASK_ID查询用户角色及任务状态
     * @param userId
     * @param taskId
     * @return
     */
    RoleTaskVO findRoleTaskInfo(@Param("userId") String userId, @Param("taskId") String taskId);



}
