package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author hejr
 * @since 2019-03-14
 */
@TableName("TB_INSTRUCT_FEEDBACK")
@Data
@ToString
public class InstructFeedback extends Model<InstructFeedback> {

    private static final long serialVersionUID = 1L;

    /**
     * 反馈ID
     */
    @TableField("FEEDBACK_ID")
    private String feedbackId;

    /**
     * 指示ID
     */
    @TableField("INSTRUCT_ID")
    private String instructId;

    /**
     * 人员ID
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 角色ID
     */
    @TableField("ROLE_ID")
    private Integer roleId;


    /**
     * 反馈内容
     */
    @TableField("FEEDBACK_CONTENT")
    private String feedbackContent;

    /**
     * 反馈时间
     */
    @TableField("FEEDBACK_TIME")
    private String feedbackTime;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
