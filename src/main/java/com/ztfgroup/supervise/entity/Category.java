package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 项目板块表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_CATEGORY")
@Data
@ToString
public class Category extends Model<Category> {

    private static final long serialVersionUID = 1L;

    /**
     * 项目板块ID
     */
    @TableId("CATEGORY_ID")
    private String categoryId;

    /**
     * 项目板块名称
     */
    @TableField("CATEGORY_NAME")
    private String categoryName;


    /**
     * 排序字段
     */
    @TableField("ORDER_NUM")
    private String orderNum;


    @Override
    protected Serializable pkVal() {
        return this.categoryId;
    }
}
