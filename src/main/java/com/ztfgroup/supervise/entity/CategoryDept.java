package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 项目板块部门表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_CATEGORY_DEPT")
@Data
@ToString
public class CategoryDept extends Model<CategoryDept> {

    private static final long serialVersionUID = 1L;

    /**
     * 项目板块ID
     */
    @TableId("CATEGORY_ID")
    private String categoryId;

    /**
     * 部门ID
     */
    @TableField("DEPT_ID")
    private String deptId;

    @Override
    protected Serializable pkVal() {
        return this.categoryId;
    }

}
