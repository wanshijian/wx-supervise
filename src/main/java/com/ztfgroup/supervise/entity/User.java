package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 人员表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_USER")
@Data
@ToString
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    /**
     * 人员ID
     */
    @TableId("USER_ID")
    private String userId;

    /**
     * 人员名称
     */
    @TableField("USER_NAME")
    private String userName;

    /**
     * 人员级别
     */
    @TableField("LV")
    private Integer lv;

    /**
     * 级别名称
     */
    @TableField("LV_NAME")
    private String lvName;

    /**
     * 微信用户编号
     */
    @TableField("WX_USER_ID")
    private String wxUserId;

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }
    
}
