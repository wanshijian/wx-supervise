package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 人员任务表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_USER_TASK")
@Data
@ToString
public class UserTask extends Model<UserTask> {

    private static final long serialVersionUID = 1L;

    /**
     * 任务ID
     */
    @TableField("TASK_ID")
    private String taskId;

    /**
     * 负责人员ID
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 人员任务角色
     */
    @TableField("ROLE")
    private Integer role;

    @Override
    protected Serializable pkVal() {
        return null;
    }
    
}
