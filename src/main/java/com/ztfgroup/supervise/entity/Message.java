package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author hejr
 * @since 2019-03-07
 */
@TableName("TB_MESSAGE")
@Data
@ToString
public class Message extends Model<Message> {

    private static final long serialVersionUID = 1L;

    /**
     * 消息ID
     */
    @TableField("ID")
    private String id;

    /**
     * 消息类型
     * G：重点工作
     * D：董事长指示
     */
    @TableField("TYPE")
    private String type;

    /**
     * 发送时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @TableField("SEND_TIME")
    private String sendTime;

    /**
     * 发送人
     */
    @TableField("SENDER")
    private String sender;

    /**
     * 接收人
     * 多个以|隔开
     */
    @TableField("RECEIVER")
    private String receiver;

    /**
     * 发送内容
     */
    @TableField("CONTENT")
    private String content;

    /**
     * 任务ID
     */
    @TableField("TASK_ID")
    private String taskId;

    /**
     * 发送状态
     */
    @TableField("STATUS")
    private String status;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
