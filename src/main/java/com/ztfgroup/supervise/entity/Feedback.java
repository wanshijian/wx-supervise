package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 任务跟踪表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_FEEDBACK")
@Data
@ToString
public class Feedback extends Model<Feedback> {

    private static final long serialVersionUID = 1L;

    /**
     * 反馈ID
     */
    @TableId("FEEDBACK_ID")
    private String feedbackId;

    /**
     * 任务ID
     */
    @TableField("TASK_ID")
    private String taskId;

    /**
     * 人员ID
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 任务状态
     */
    @TableField("TASK_STATUS")
    private String taskStatus;

    /**
     * 反馈内容
     */
    @TableField("FEEDBACK_CONTENT")
    private String feedbackContent;

    /**
     * 反馈时间
     */
    @TableField("FEEDBACK_TIME")
    private String feedbackTime;

    @Override
    protected Serializable pkVal() {
        return this.feedbackId;
    }

}
