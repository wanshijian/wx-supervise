package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.*;
import lombok.Data;

import java.util.List;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/2/28 17:00
 */
@Data
public class UserTaskVO extends UserTask {


    private Task task;
    private List<User> userList;
    private Dept dept;
    private UserTask userTask;
    private Category category;


}
