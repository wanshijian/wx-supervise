package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.Task;
import com.ztfgroup.supervise.entity.UserTask;
import lombok.Data;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/2/28 17:00
 */
@Data
public class RoleTaskVO extends UserTask {


    private Task task;


}
