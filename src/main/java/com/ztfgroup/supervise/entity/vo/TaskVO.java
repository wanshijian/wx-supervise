package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.Dept;
import com.ztfgroup.supervise.entity.Feedback;
import com.ztfgroup.supervise.entity.Task;
import lombok.Data;

import java.util.List;
@Data
public class TaskVO extends Task {

    private Category category;
    private Dept dept;
    private Feedback feedback;
    private List<UserVO> userVoList;
}
