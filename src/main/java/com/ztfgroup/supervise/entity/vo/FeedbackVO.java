package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.Feedback;
import com.ztfgroup.supervise.entity.User;
import com.ztfgroup.supervise.entity.UserTask;
import lombok.Data;

@Data
public class FeedbackVO extends Feedback {

    private UserTask userTask;
    private User user;
}
