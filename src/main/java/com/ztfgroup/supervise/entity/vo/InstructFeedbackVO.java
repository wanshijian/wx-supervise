package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.InstructFeedback;
import com.ztfgroup.supervise.entity.User;
import lombok.Data;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/3/14 14:24
 */
@Data
public class InstructFeedbackVO extends InstructFeedback {

    private User user;
}
