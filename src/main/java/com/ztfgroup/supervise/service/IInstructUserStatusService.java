package com.ztfgroup.supervise.service;

import com.ztfgroup.supervise.entity.InstructUserStatus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 指示用户记录表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-03-06
 */
public interface IInstructUserStatusService extends IService<InstructUserStatus> {


    /**
     * 处理当前指示，若已读则不做处理，未读：则新增
     * @param selfUserId
     * @param instructId
     */
    void dealWithInstructInfo(String selfUserId, String instructId);

}
