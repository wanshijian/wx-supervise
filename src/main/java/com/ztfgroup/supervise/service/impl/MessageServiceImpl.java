package com.ztfgroup.supervise.service.impl;

import com.ztfgroup.supervise.entity.Message;
import com.ztfgroup.supervise.mapper.MessageMapper;
import com.ztfgroup.supervise.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-03-07
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {

}
