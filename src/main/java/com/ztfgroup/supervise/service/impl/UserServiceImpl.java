package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztfgroup.supervise.entity.User;
import com.ztfgroup.supervise.mapper.UserMapper;
import com.ztfgroup.supervise.service.IUserService;
import com.ztfgroup.supervise.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 人员表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public List<User> findAllTaskUser(String userId, String categoryId, String deptId) {
        return baseMapper.findTaskUser(userId, categoryId, deptId);
    }

    @Override
    public List<User> findAllUser(String categoryIds, String deptIds) {
        return baseMapper.findAllUser(StringUtil.convertQueryIds(categoryIds), StringUtil.convertQueryIds(deptIds));
    }

    @Override
    public User findRoleByUserId(String userId) {
        return baseMapper.findRoleByUserId(userId);
    }
}
