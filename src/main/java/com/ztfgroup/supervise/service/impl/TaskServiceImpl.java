package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztfgroup.supervise.entity.Task;
import com.ztfgroup.supervise.entity.vo.TaskVO;
import com.ztfgroup.supervise.mapper.TaskMapper;
import com.ztfgroup.supervise.service.ITaskService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 任务清单表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {

    @Override
    public List<TaskVO> findAllTaskBySelfUserId(String selfUserId) {
        return baseMapper.findTaskByUserIdCategoryIdDeptId(selfUserId, null, null, null);
    }

    @Override
    public List<TaskVO>
    findTaskBySelfUserIdOtherUserIdCategoryIdDeptId(String selfUserId, String otherUserId, String categoryId, String deptId) {
        List<TaskVO> taskVOList = baseMapper.findTaskByUserIdCategoryIdDeptId(selfUserId, otherUserId, categoryId, deptId);
        return taskVOList;
    }


    @Override
    public TaskVO findTaskByTaskId(String taskId) {
        return baseMapper.findTaskByTaskId(taskId);
    }
}
