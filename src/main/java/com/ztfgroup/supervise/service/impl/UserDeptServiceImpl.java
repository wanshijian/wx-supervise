package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztfgroup.supervise.entity.UserDept;
import com.ztfgroup.supervise.mapper.UserDeptMapper;
import com.ztfgroup.supervise.service.IUserDeptService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 人员部门表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@Service
public class UserDeptServiceImpl extends ServiceImpl<UserDeptMapper, UserDept> implements IUserDeptService {

}
