package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.UserDept;

/**
 * <p>
 * 人员部门表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface IUserDeptService extends IService<UserDept> {

}
