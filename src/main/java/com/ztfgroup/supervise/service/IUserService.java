package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.User;

import java.util.List;

/**
 * <p>
 * 人员表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface IUserService extends IService<User> {


    /**
     * 以当前用户USER_ID，查询其所能见的其他用户信息
     * @param userId
     * @param categoryId
     * @param deptId
     * @return
     */
    List<User> findAllTaskUser(String userId, String categoryId, String deptId);


    /**
     * 根据板块ID和公司ID获取对应的用户信息
     * @param categoryIds
     * @param deptIds
     * @return
     */
    List<User> findAllUser(String categoryIds, String deptIds);

    /**
     * 根据人员ID获取角色
     * @param userId
     * @return
     */
    User findRoleByUserId(String userId);
}
