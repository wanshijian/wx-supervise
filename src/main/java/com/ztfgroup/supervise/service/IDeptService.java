package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.Dept;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface IDeptService extends IService<Dept> {

    /**
     * 按用户USER_ID查询其所能见的部门
     *
     * @param userId 用户USER_ID
     * @return 部门
     */
    List<Dept> findAllTaskDept(String userId);

    /**
     * 按用户USER_ID  项目板块CATEGORY_ID查询其所能见的部门
     *
     * @param userId     用户USER_ID
     * @param categoryId 项目板块CATEGORY_ID
     * @return 部门
     */
    List<Dept> findTaskDept(String userId, String categoryId);


    List<Dept> findAllDept(String categoryIds);
}
