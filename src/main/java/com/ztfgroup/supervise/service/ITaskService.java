package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.Task;
import com.ztfgroup.supervise.entity.vo.TaskVO;

import java.util.List;

/**
 * <p>
 * 任务清单表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface ITaskService extends IService<Task> {

    /**
     * 按当前用户USER_ID查询任务列表
     *
     * @param selfUserId 当前用户USER_ID
     * @return 任务列表
     */
    List<TaskVO> findAllTaskBySelfUserId(String selfUserId);

    /**
     * 按当前用户USER_ID  用户USER_ID  项目板块CATEGORY_ID  部门DEPT_ID查询任务列表
     *
     * @param selfUserId  当前用户USER_ID
     * @param otherUserId 用户USER_ID
     * @param categoryId  项目板块CATEGORY_ID
     * @param deptId      部门DEPT_ID
     * @return 任务列表
     */
    List<TaskVO> findTaskBySelfUserIdOtherUserIdCategoryIdDeptId(String selfUserId, String otherUserId, String categoryId, String deptId);


    /**
     * 按任务TASK_ID任务
     *
     * @param taskId 任务TASK_ID
     * @return 任务
     */
    TaskVO findTaskByTaskId(String taskId);
}
