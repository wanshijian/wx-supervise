package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.UserTask;
import com.ztfgroup.supervise.entity.vo.RoleTaskVO;
import com.ztfgroup.supervise.entity.vo.UserTaskVO;

import java.util.List;

/**
 * <p>
 * 人员任务表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface IUserTaskService extends IService<UserTask> {


    /**
     * 根据USER_ID和TASK_ID获取角色及任务状态信息
     *
     * @param userId
     * @param taskId
     * @return
     */
    RoleTaskVO findRoleTaskInfo(String userId, String taskId);


    /**
     * 根据人员ID和任务ID获取角色信息
     *
     * @param userId
     * @param taskId
     * @return
     */
    List<UserTaskVO> findUserInfoByTaskId(String userId, String taskId);


}
