package com.ztfgroup.supervise.util;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.HashMap;
import java.util.Map;

public class Constants {


    public static final Integer WX_SUCCESS_CODE = 0;

    public static final String USER_ID = "user_id";


    public static final String LEADER_WECHAT_ID = "HeJinRong";


    public static final String FEEDBACK_ID_PREX = "FB-";
    public static final String INSTRUCT_FEEDBACK_ID_PREX = "IS_FB-";
    public static final String INSTRUCT_ID_PREX = "IS-";
    public static final String MESSAGE_ID_PREX = "MS-";
    public static final String PERSON_LIABLE_ROLE = "4";
    public static final String TASK_FINISH_STATUS = "F";

    public static final int MAX_CONTENT_LENGTH = 50;
    public static final Integer LEADER_ROLE_ID = 1;


    public static final String LAST_WEEK_TYPE = "1";
    public static final String LAST_MONTH_TYPE = "2";
    public static final String A_MONTH_AGO = "3";

    public static final String COMMA_SEPARATE_CHAR = ",";
    public static final String LINE_SEPARATE_CHAR = "|";

    public static final Integer ONE_WEEK_DAY = 7;
    public static final Integer ONE_MONTH_DAY = 30;


    /**
     * 时间格式化格式
     */
    public static final FastDateFormat DATE_FORMAT_SPECIAL_DAY = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");
    public static final FastDateFormat DATE_FORMAT_SEND_TIME = FastDateFormat.getInstance("yyyy-MM-dd HH:mm");
    public static final FastDateFormat DATE_FORMAT_FINISH_TIME = FastDateFormat.getInstance("yyyy-MM-dd");


    public static final Map<Integer, String> ROLE_MAP = new HashMap<>();

    static {
        ROLE_MAP.put(1, "提出人");
        ROLE_MAP.put(2, "干系人");
        ROLE_MAP.put(3, "集团领导");
        ROLE_MAP.put(4, "责任人");
    }

    public static final Map<Integer, String> ROLE_MSG_MAP = new HashMap<>();

    static {
        ROLE_MSG_MAP.put(1, "要求");
        ROLE_MSG_MAP.put(2, "意见");
        ROLE_MSG_MAP.put(3, "意见");
        ROLE_MSG_MAP.put(4, "反馈");
    }


    public static final Map<String, String> FEEDBACK_STATUS_MAP = new HashMap<>();

    static {
        FEEDBACK_STATUS_MAP.put("D", "延期");
        FEEDBACK_STATUS_MAP.put("G", "进行中");
        FEEDBACK_STATUS_MAP.put("P", "暂停");
        FEEDBACK_STATUS_MAP.put("F", "已完成");
    }

    /**
     * 发送微信消息模板
     */
    public static final String WECHAT_MSG_TEMPLATE = "<div class=\"gray\">${dateTime}<br></div>"
            + "<div class=\"normal\">${categoryName}-${deptName}-${taskName}<br></div>"
            + "<div class=\"normal\">【${roleName}-${userName}】有新的${suggest}：</div>"
            + "<div class=\"highlight\">${feedbackContent}</div>";

    /**
     * 指示消息模板
     */
    public static final String INSTRUCT_MSG_TEMPLATE = "<div class=\"gray\">${dateTime}<br></div>"
            + "<div class=\"normal\">董事长有新的指示，点击查看。<br></div>";


    public static final String INSTRUCT_FEEDBACK_MSG_TEMPLATE = "<div class=\"gray\">${dateTime}<br></div>"
            + "<div class=\"normal\">${userName}有新的${roleId}，点击查看。<br></div>";

}
