package com.ztfgroup.supervise.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/3/7 18:27
 */
public class StringUtil {


    /**
     * 处理id加引号，如：001,002,003 -> '001','002','003'
     *
     * @param ids
     */
    public static String convertQueryIds(String ids) {
        String result = "";
        if (StringUtils.isNotEmpty(ids)) {
            StringBuffer sb = new StringBuffer();
            String[] idArr = ids.split(Constants.COMMA_SEPARATE_CHAR);
            for (String id : idArr) {
                sb.append("'").append(id).append("',");
            }
            result = sb.toString().substring(0, sb.toString().length() - 1);
        }
        return result;
    }
}
