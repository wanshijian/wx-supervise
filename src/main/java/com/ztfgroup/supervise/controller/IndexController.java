package com.ztfgroup.supervise.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ztfgroup.supervise.common.SuperController;
import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.User;
import com.ztfgroup.supervise.entity.vo.CategoryVO;
import com.ztfgroup.supervise.entity.vo.FeedbackVO;
import com.ztfgroup.supervise.entity.vo.RoleTaskVO;
import com.ztfgroup.supervise.entity.vo.TaskVO;
import com.ztfgroup.supervise.intercept.RequiredWxOauth2;
import com.ztfgroup.supervise.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author: hejr
 * @desc: 业务控制层
 * @date: 2019/2/26 18:45
 */
@Controller
@Slf4j
public class IndexController extends SuperController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IFeedbackService feedbackService;

    @Autowired
    private IUserTaskService userTaskService;


    /**
     * 首页加载信息，页面一开始进来
     *
     * @param model
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/")
    public String index(Model model) {
        List<Category> categoryList = categoryService.findAllTaskCategory(getSessionUserInfo());
        model.addAttribute("categoryList", categoryList);
        return "index";
    }

    /**
     * 加载当前登录用户能看到的板块信息
     *
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/findTaskAndCategoryList")
    public Object findTaskAndCategoryList(String otherUserId, String categoryId, String deptId) {
        List<TaskVO> taskVoList = taskService.findTaskBySelfUserIdOtherUserIdCategoryIdDeptId(getSessionUserInfo(), otherUserId, categoryId, deptId);
        return taskVoList;
    }

    /**
     * 加载任务信息，状态，最新反馈
     *
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/findTaskAndNewestFeedback")
    public Object findTaskAndNewestFeedback(String deptId, String otherUserId) {
        List<CategoryVO> categoryVOList = categoryService.findTaskAndNewestFeedback(getSessionUserInfo(), deptId, otherUserId);
        return categoryVOList;
    }


    /**
     * 加载部门信息
     *
     * @param categoryId 板块ID
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/findDeptList")
    public Object findDeptList(String categoryId) {
        List<CategoryVO> categoryVOList = categoryService.findCategoryDeptList(getSessionUserInfo(), categoryId);
        return categoryVOList;
    }

    /**
     * 加载用户信息
     *
     * @param categoryId
     * @param deptId
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/findUserList")
    public Object findUserList(String categoryId, String deptId) {
        return userService.findAllTaskUser(getSessionUserInfo(), categoryId, deptId);
    }

    /**
     * 跳转到任务详情页面
     *
     * @param taskId
     * @param model
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/detail")
    public String findTaskDetail(String taskId, Model model) {
        RoleTaskVO roleTaskVO = userTaskService.findRoleTaskInfo(getSessionUserInfo(), taskId);
        if (roleTaskVO != null) {
            model.addAttribute("taskId", taskId);
            return "detail";
        } else {
            return redirectTo("errorpage");
        }
    }

    /**
     * 获取板块任务详情
     *
     * @param taskId
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping(value = "/findTaskDetailInfo")
    public Object findTaskDetailInfo(String taskId) {
        TaskVO taskVo = taskService.findTaskByTaskId(taskId);
        return taskVo;
    }

    /**
     * 获取反馈信息
     *
     * @param taskId
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping(value = "/findFeedBackInfo")
    public Object findFeedBackInfo(String taskId) {
        List<FeedbackVO> feedbackVOList = feedbackService.findFeedbackByTaskId(taskId);
        return feedbackVOList;
    }

    /**
     * 获取用户角色及任务状态
     *
     * @param taskId
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping(value = "/findUserRoleInfo")
    public Object findUserRoleInfo(String taskId) {
        RoleTaskVO userTask = userTaskService.findRoleTaskInfo(getSessionUserInfo(), taskId);
        return userTask;
    }

    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping(value = "/findUserName")
    public Object findUserName() {
        User user = userService.selectOne(new QueryWrapper<User>().eq("USER_ID", getSessionUserInfo()));
        return user;
    }


    /**
     * 新增反馈信息
     *
     * @param roleId
     * @param taskId
     * @param taskStatus
     * @param feedbackContent
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/addFeedback")
    public Object addFeedback(@RequestParam(value = "roleId") String roleId, @RequestParam(value = "taskId") String taskId, @RequestParam(value = "taskStatus") String taskStatus, @RequestParam(value = "feedbackContent") String feedbackContent) {
        Integer result = -1;
        try {
            result = feedbackService.addFeeback(roleId, getSessionUserInfo(), taskId, taskStatus, feedbackContent);
        } catch (Exception e) {
            log.error("新增反馈信息异常. 参数：roleId：{}, taskId：{}", roleId, taskId);
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 跳转无权限页面
     *
     * @return
     */
    @RequestMapping(value = "/errorpage")
    public String findUserRoleInfo() {
        return "errorpage";
    }

}
