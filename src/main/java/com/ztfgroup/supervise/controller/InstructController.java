package com.ztfgroup.supervise.controller;

import com.ztfgroup.supervise.common.SuperController;
import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.Dept;
import com.ztfgroup.supervise.entity.User;
import com.ztfgroup.supervise.entity.vo.InstructFeedbackVO;
import com.ztfgroup.supervise.entity.vo.InstructVO;
import com.ztfgroup.supervise.intercept.RequiredWxOauth2;
import com.ztfgroup.supervise.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author: hejr
 * @desc: 指示控制层
 * @date: 2019/3/6 14:08
 */
@Controller
@Slf4j
public class InstructController extends SuperController {


    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private IDeptService deptService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IInstructService instructService;

    @Autowired
    private IInstructUserStatusService instructUserStatusService;

    @Autowired
    private IInstructFeedbackService instructFeedbackService;

    /**
     * 首页加载信息
     *
     * @param model
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/instruct")
    public String index(Model model) {
        //判断当前用户是否可以进入指示页面，默认只有董事长可以进来看到所有
        User user = userService.findRoleByUserId(getSessionUserInfo());
        if (user != null && user.getLv() != null) {
            model.addAttribute("currentUserRole", user.getLv());
        }
        return "instruct";
    }


    /**
     * 查询板块信息
     *
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/findAllCategory")
    @ResponseBody
    public Object findAllCategory(@RequestParam(required = false) String currentUserRoleId) {
        List<Category> categoryList = categoryService.findAllCategory();
        return categoryList;
    }


    /**
     * 根据板块ID查询公司信息
     *
     * @param categoryIds 板块ID
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/findAllDept")
    @ResponseBody
    public Object findAllDept(String categoryIds) {
        List<Dept> deptList = deptService.findAllDept(categoryIds);
        return deptList;

    }


    /**
     * 根据板块ID和公司ID获取用户
     *
     * @param categoryIds 板块ID
     * @param deptIds     公司ID
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/findAllUser")
    @ResponseBody
    public Object findAllUser(String categoryIds, String deptIds) {
        List<User> userList = userService.findAllUser(categoryIds, deptIds);
        return userList;
    }


    /**
     * 获取历史指示信息
     *
     * @param categoryId 板块ID
     * @param deptId     公司ID
     * @param userId     用户ID
     * @param dateType   日期类型 1：最近一周 2：最近一月 3：一个月以前
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/findAllInstruct")
    @ResponseBody
    public Object findAllInstruct(String categoryId, String deptId, String userId, @RequestParam(value = "dateType", defaultValue = "1") String dateType, Integer currentUserRoleId) {
        List<InstructVO> instructVOList = instructService.findAllInstruct(categoryId, deptId, userId, dateType, currentUserRoleId, getSessionUserInfo());
        return instructVOList;
    }


    /**
     * 新增指示
     *
     * @param categoryIds 板块ID
     * @param deptIds     公司ID
     * @param userIds     用户ID
     * @param content     指示内容
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/addInstruct")
    @ResponseBody
    public Object addInstruct(String categoryIds, String deptIds, String userIds, String content) {
        Integer ret = instructService.addInstruct(categoryIds, deptIds, userIds, content, getSessionUserInfo());
        return ret;
    }

    /**
     * 跳转到指示详情页，判断是否有权限
     *
     * @param instructId 指示ID
     * @param model
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/instructdetail")
    public String instructDetail(String instructId, Model model) {
        //先判断该条指示当前用户是否可以查看，如果不能直接跳转无权限页面
        Boolean isOk = instructService.judgeIsScanInstruct(getSessionUserInfo(), instructId);
        if (!isOk) {
            return redirectTo("errorpage");
        } else {
            //处理指示是否已读
            instructUserStatusService.dealWithInstructInfo(getSessionUserInfo(), instructId);
            model.addAttribute("instructId", instructId);
            return "instructdetail";
        }
    }

    /**
     * 点击历史指示，跳转到详情页
     * @param instructId
     * @param model
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/goInstructDetailPage")
    public String goInstructDetailPage(String instructId, Model model) {
        if (StringUtils.isNotEmpty(instructId)) {
            //处理指示是否已读
            instructUserStatusService.dealWithInstructInfo(getSessionUserInfo(), instructId);
            model.addAttribute("instructId", instructId);
            return "instructdetail";
        } else {
            return redirectTo("errorpage");
        }
    }

    /**
     * 获取单条指示消息
     *
     * @param instructId 指示ID
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/findInstructById")
    @ResponseBody
    public Object findInstructById(String instructId) {
        InstructVO instructVO = instructService.findInstructAndUserNameById(instructId);
        return instructVO;
    }


    /**
     * 按指示ID查询反馈进度信息
     *
     * @param instructId
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/findInstructFeedBack")
    @ResponseBody
    public Object findInstructFeedBack(String instructId) {
        List<InstructFeedbackVO> instructFeedbackVOList = instructFeedbackService.findInstructFeedBack(instructId);
        return instructFeedbackVOList;
    }


    /**
     * 新增指示反馈
     *
     * @param instructId
     * @param feedbackContent
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/addInstructFeedback")
    public Object addInstructFeedback(@RequestParam(value = "instructId") String instructId, @RequestParam(value = "roleId") Integer roleId, @RequestParam(value = "feedbackContent") String feedbackContent) {
        Integer result = -1;
        try {
            result = instructFeedbackService.addInstructFeeback(getSessionUserInfo(), instructId, roleId, feedbackContent);
        } catch (Exception e) {
            log.error("新增指示反馈信息异常. 参数：instructId：{}", instructId);
            e.printStackTrace();
        }
        return result;
    }

    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/findCurrentUserRole")
    public Object findCurrentUserRole() {
        User user = userService.selectById(getSessionUserInfo());
        return user;
    }


}
