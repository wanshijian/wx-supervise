package com.ztfgroup.supervise.controller;

import com.ztfgroup.supervise.common.SuperController;
import com.ztfgroup.supervise.entity.vo.CategoryVO;
import com.ztfgroup.supervise.intercept.RequiredWxOauth2;
import com.ztfgroup.supervise.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
* @desc: 任务追踪控制层
* @author:hejr
* @date:2019/3/22
*/
@Controller
@Slf4j
public class TaskTrackController extends SuperController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IFeedbackService feedbackService;

    @Autowired
    private IUserTaskService userTaskService;


    /**
     * 首页加载信息，页面一开始进来
     *
     * @param model
     * @return
     */
    @RequiredWxOauth2
    @RequestMapping(value = "/tasktrack")
    public String index(Model model) {
        //判断当前用户是否有权限进入工作追踪页面
//        User user = userService.findRoleByUserId(getSessionUserInfo());
//        if (user != null && user.getLv() != null) {
//            model.addAttribute("currentUserRole", user.getLv());
//        }
        return "tasktrack";
    }


    /**
     * 查询任务追踪信息
     * @return
     */
    @RequiredWxOauth2
    @ResponseBody
    @RequestMapping("/findTaskTrackList")
    public Object findTaskTrackList() {
        List<CategoryVO> categoryVOList = categoryService.findTaskTrackList(getSessionUserInfo());
        return categoryVOList;
    }

}
