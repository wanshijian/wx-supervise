package com.ztfgroup.supervise.wxparam.impl;

import com.hszsd.weixin.api.SnsAPI;
import com.hszsd.weixin.out.GetAccessTokenOut;
import com.ztfgroup.supervise.util.Constants;
import com.ztfgroup.supervise.wxparam.AccessTokenApi;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("accessTokenApi")
public class AccessTokenImpl implements AccessTokenApi {

    @Cacheable(value = "myToken")
    @Override
    public String getAccessToken(String corpId, String corpSecret) throws Exception {
        GetAccessTokenOut getSuiteAccessTokenOut = SnsAPI.oauth2AccessToken(corpId, corpSecret);
        if (!Constants.WX_SUCCESS_CODE.equals(getSuiteAccessTokenOut.getErrcode())) {
            throw new Exception("微信获取accessToken失败：" + getSuiteAccessTokenOut.getErrcode());
        }
        return getSuiteAccessTokenOut.getAccess_token();
    }
}
