-------------------------------------------------
-- Export file for user ZTFGROUP               --
-- Created by forestxia on 2019/4/22, 14:19:33 --
-------------------------------------------------

set define off
spool wx-supervise.log

prompt
prompt Creating table TB_CATEGORY
prompt ==========================
prompt
create table ZTFGROUP.TB_CATEGORY
(
  category_id   VARCHAR2(10) not null,
  category_name VARCHAR2(100) not null,
  order_num     NUMBER
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 8K
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_CATEGORY
is '项目板块表';
comment on column ZTFGROUP.TB_CATEGORY.category_id
is '项目板块ID';
comment on column ZTFGROUP.TB_CATEGORY.category_name
is '项目板块名称';
comment on column ZTFGROUP.TB_CATEGORY.order_num
is '排序号';
alter table ZTFGROUP.TB_CATEGORY
  add constraint PK_CATEGORY_ID primary key (CATEGORY_ID)
  using index
  tablespace HSPRD
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
  initial 64K
  next 1M
  minextents 1
  maxextents unlimited
  );

prompt
prompt Creating table TB_CATEGORY_DEPT
prompt ===============================
prompt
create table ZTFGROUP.TB_CATEGORY_DEPT
(
  category_id VARCHAR2(10),
  dept_id     VARCHAR2(10)
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 1M
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_CATEGORY_DEPT
is '项目板块部门表';
comment on column ZTFGROUP.TB_CATEGORY_DEPT.category_id
is '项目板块ID';
comment on column ZTFGROUP.TB_CATEGORY_DEPT.dept_id
is '部门ID';

prompt
prompt Creating table TB_DEPT
prompt ======================
prompt
create table ZTFGROUP.TB_DEPT
(
  dept_id   VARCHAR2(10) not null,
  dept_name VARCHAR2(100) not null,
  order_num NUMBER
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 8K
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_DEPT
is '部门表';
comment on column ZTFGROUP.TB_DEPT.dept_id
is '部门ID';
comment on column ZTFGROUP.TB_DEPT.dept_name
is '部门名称';
comment on column ZTFGROUP.TB_DEPT.order_num
is '排序号';
alter table ZTFGROUP.TB_DEPT
  add constraint PK_DEPT_ID primary key (DEPT_ID)
  using index
  tablespace HSPRD
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
  initial 64K
  next 1M
  minextents 1
  maxextents unlimited
  );

prompt
prompt Creating table TB_FEEDBACK
prompt ==========================
prompt
create table ZTFGROUP.TB_FEEDBACK
(
  feedback_id      VARCHAR2(10) not null,
  task_id          VARCHAR2(10) not null,
  user_id          VARCHAR2(10) not null,
  task_status      VARCHAR2(32),
  feedback_content VARCHAR2(1000) not null,
  feedback_time    VARCHAR2(20) not null
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 8K
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_FEEDBACK
is '任务跟踪表';
comment on column ZTFGROUP.TB_FEEDBACK.feedback_id
is '反馈ID';
comment on column ZTFGROUP.TB_FEEDBACK.task_id
is '任务ID';
comment on column ZTFGROUP.TB_FEEDBACK.user_id
is '人员ID';
comment on column ZTFGROUP.TB_FEEDBACK.task_status
is '任务状态';
comment on column ZTFGROUP.TB_FEEDBACK.feedback_content
is '反馈内容';
comment on column ZTFGROUP.TB_FEEDBACK.feedback_time
is '反馈时间';
alter table ZTFGROUP.TB_FEEDBACK
  add constraint PK_FEEDBACK_ID primary key (FEEDBACK_ID)
  using index
  tablespace HSPRD
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
  initial 64K
  next 1M
  minextents 1
  maxextents unlimited
  );

prompt
prompt Creating table TB_INSTRUCT
prompt ==========================
prompt
create table ZTFGROUP.TB_INSTRUCT
(
  instruct_id      VARCHAR2(10) not null,
  time             VARCHAR2(20),
  instruct_content VARCHAR2(1000),
  category_ids     VARCHAR2(200),
  dept_ids         VARCHAR2(400),
  user_ids         VARCHAR2(500)
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 1M
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_INSTRUCT
is '指示表';
comment on column ZTFGROUP.TB_INSTRUCT.instruct_id
is '指示ID';
comment on column ZTFGROUP.TB_INSTRUCT.time
is '录入时间';
comment on column ZTFGROUP.TB_INSTRUCT.instruct_content
is '指示内容';
comment on column ZTFGROUP.TB_INSTRUCT.category_ids
is '项目板块';
comment on column ZTFGROUP.TB_INSTRUCT.dept_ids
is '部门';
comment on column ZTFGROUP.TB_INSTRUCT.user_ids
is '人员';
alter table ZTFGROUP.TB_INSTRUCT
  add constraint PK_INSTRUCT_ID primary key (INSTRUCT_ID)
  using index
  tablespace HSPRD
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
  initial 64K
  next 1M
  minextents 1
  maxextents unlimited
  );

prompt
prompt Creating table TB_INSTRUCT_FEEDBACK
prompt ===================================
prompt
create table ZTFGROUP.TB_INSTRUCT_FEEDBACK
(
  feedback_id      VARCHAR2(10) not null,
  instruct_id      VARCHAR2(10) not null,
  user_id          VARCHAR2(10) not null,
  feedback_content VARCHAR2(1000) not null,
  feedback_time    VARCHAR2(20) not null,
  role_id          NUMBER not null
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 1M
minextents 1
maxextents unlimited
);
comment on column ZTFGROUP.TB_INSTRUCT_FEEDBACK.feedback_id
is '反馈ID';
comment on column ZTFGROUP.TB_INSTRUCT_FEEDBACK.instruct_id
is '指示ID';
comment on column ZTFGROUP.TB_INSTRUCT_FEEDBACK.user_id
is '人员ID';
comment on column ZTFGROUP.TB_INSTRUCT_FEEDBACK.feedback_content
is '反馈内容';
comment on column ZTFGROUP.TB_INSTRUCT_FEEDBACK.feedback_time
is '反馈时间';
comment on column ZTFGROUP.TB_INSTRUCT_FEEDBACK.role_id
is '角色ID';

prompt
prompt Creating table TB_INSTRUCT_USER_STATUS
prompt ======================================
prompt
create table ZTFGROUP.TB_INSTRUCT_USER_STATUS
(
  instruct_id VARCHAR2(10),
  user_id     VARCHAR2(10),
  read_time   VARCHAR2(20)
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 1M
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_INSTRUCT_USER_STATUS
is '指示用户记录表';
comment on column ZTFGROUP.TB_INSTRUCT_USER_STATUS.instruct_id
is '指示ID';
comment on column ZTFGROUP.TB_INSTRUCT_USER_STATUS.user_id
is '人员ID';
comment on column ZTFGROUP.TB_INSTRUCT_USER_STATUS.read_time
is '已读时间';

prompt
prompt Creating table TB_MESSAGE
prompt =========================
prompt
create table ZTFGROUP.TB_MESSAGE
(
  id        VARCHAR2(10) not null,
  type      VARCHAR2(20) not null,
  send_time VARCHAR2(20) not null,
  sender    VARCHAR2(50) not null,
  receiver  VARCHAR2(1000),
  content   VARCHAR2(1000),
  task_id   VARCHAR2(10),
  status    VARCHAR2(10)
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 8K
minextents 1
maxextents unlimited
);
comment on column ZTFGROUP.TB_MESSAGE.id
is '消息ID';
comment on column ZTFGROUP.TB_MESSAGE.type
is '消息类型
G：重点工作
D：董事长指示
';
comment on column ZTFGROUP.TB_MESSAGE.send_time
is '发送时间
格式：yyyy-MM-dd HH:mm:ss
';
comment on column ZTFGROUP.TB_MESSAGE.sender
is '发送人';
comment on column ZTFGROUP.TB_MESSAGE.receiver
is '接收人
多个以|隔开
';
comment on column ZTFGROUP.TB_MESSAGE.content
is '发送内容';
comment on column ZTFGROUP.TB_MESSAGE.task_id
is '任务ID';
comment on column ZTFGROUP.TB_MESSAGE.status
is '发送状态';

prompt
prompt Creating table TB_TASK
prompt ======================
prompt
create table ZTFGROUP.TB_TASK
(
  task_id      VARCHAR2(10) not null,
  task_name    VARCHAR2(500) not null,
  task_content VARCHAR2(1000) not null,
  dept_id      VARCHAR2(10),
  category_id  VARCHAR2(10) not null,
  frequency    VARCHAR2(10),
  start_time   VARCHAR2(1000) not null,
  close_time   VARCHAR2(1000),
  finish_time  VARCHAR2(1000),
  status       VARCHAR2(1),
  order_num    NUMBER
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 8K
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_TASK
is '任务清单表';
comment on column ZTFGROUP.TB_TASK.task_id
is '事项ID';
comment on column ZTFGROUP.TB_TASK.task_name
is '事项名称';
comment on column ZTFGROUP.TB_TASK.task_content
is '事项详情';
comment on column ZTFGROUP.TB_TASK.dept_id
is '部门ID';
comment on column ZTFGROUP.TB_TASK.category_id
is '项目(板块)ID';
comment on column ZTFGROUP.TB_TASK.frequency
is '事项反馈频次';
comment on column ZTFGROUP.TB_TASK.start_time
is '事项录入时间';
comment on column ZTFGROUP.TB_TASK.close_time
is '事项截止时间';
comment on column ZTFGROUP.TB_TASK.finish_time
is '完成时间';
comment on column ZTFGROUP.TB_TASK.status
is '状态';
comment on column ZTFGROUP.TB_TASK.order_num
is '排序';
alter table ZTFGROUP.TB_TASK
  add constraint PK_TASK_ID primary key (TASK_ID)
  using index
  tablespace HSPRD
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
  initial 64K
  next 1M
  minextents 1
  maxextents unlimited
  );

prompt
prompt Creating table TB_USER
prompt ======================
prompt
create table ZTFGROUP.TB_USER
(
  user_id    VARCHAR2(10) not null,
  user_name  VARCHAR2(50) not null,
  lv         NUMBER not null,
  lv_name    VARCHAR2(50) not null,
  wx_user_id VARCHAR2(255)
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 8K
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_USER
is '人员表';
comment on column ZTFGROUP.TB_USER.user_id
is '人员ID';
comment on column ZTFGROUP.TB_USER.user_name
is '人员名称';
comment on column ZTFGROUP.TB_USER.lv
is '人员级别';
comment on column ZTFGROUP.TB_USER.lv_name
is '级别名称';
comment on column ZTFGROUP.TB_USER.wx_user_id
is '微信用户编号';
alter table ZTFGROUP.TB_USER
  add constraint PK_USER_ID primary key (USER_ID)
  using index
  tablespace HSPRD
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
  initial 64K
  next 1M
  minextents 1
  maxextents unlimited
  );

prompt
prompt Creating table TB_USER_DEPT
prompt ===========================
prompt
create table ZTFGROUP.TB_USER_DEPT
(
  user_id VARCHAR2(10) not null,
  dept_id VARCHAR2(10) not null
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 1M
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_USER_DEPT
is '人员部门表';
comment on column ZTFGROUP.TB_USER_DEPT.user_id
is '人员ID';
comment on column ZTFGROUP.TB_USER_DEPT.dept_id
is '部门ID';

prompt
prompt Creating table TB_USER_TASK
prompt ===========================
prompt
create table ZTFGROUP.TB_USER_TASK
(
  task_id VARCHAR2(10) not null,
  user_id VARCHAR2(10) not null,
  role    NUMBER not null
)
tablespace HSPRD
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 8K
minextents 1
maxextents unlimited
);
comment on table ZTFGROUP.TB_USER_TASK
is '人员任务表';
comment on column ZTFGROUP.TB_USER_TASK.task_id
is '任务ID';
comment on column ZTFGROUP.TB_USER_TASK.user_id
is '负责人员ID';
comment on column ZTFGROUP.TB_USER_TASK.role
is '人员任务角色';

prompt
prompt Creating sequence SEQ_TB_FEEDBACK
prompt =================================
prompt
create sequence ZTFGROUP.SEQ_TB_FEEDBACK
minvalue 1
maxvalue 9999999999999999999999999999
start with 321
increment by 1
cache 20;

prompt
prompt Creating sequence SEQ_TB_INSTRUCT
prompt =================================
prompt
create sequence ZTFGROUP.SEQ_TB_INSTRUCT
minvalue 1
maxvalue 9999999999999999999999999999
start with 321
increment by 1
cache 20;

prompt
prompt Creating sequence SEQ_TB_INSTRUCT_FEEDBACK
prompt ==========================================
prompt
create sequence ZTFGROUP.SEQ_TB_INSTRUCT_FEEDBACK
minvalue 1
maxvalue 9999999999999999999999999999
start with 301
increment by 1
cache 20;

prompt
prompt Creating sequence SEQ_TB_MESSAGE
prompt ================================
prompt
create sequence ZTFGROUP.SEQ_TB_MESSAGE
minvalue 1
maxvalue 9999999999999999999999999999
start with 181
increment by 1
cache 20;


spool off
