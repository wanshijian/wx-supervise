<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
    <title>工作详情</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/shuiyin.js"></script>
</head>

<body>
<style>
    *{
        padding: 0;
        margin: 0;
        -moz-user-select:none;/*火狐*/
        -webkit-user-select:none;/*webkit浏览器*/
        -ms-user-select:none;/*IE10*/
        -khtml-user-select:none;/*早期浏览器*/
        user-select:none;
    }
    .content{
        width: 100%;
        min-height: 100vh;
        overflow: hidden;
        background-color: #f0f2f9;
        padding: 10px;
    }
    .taskInfo{
        background-color: #ffffff;
        min-height: 100px;
        padding: 10px;
    }
    .titleName{
        color: #171b6d;
        font-size: 18px;
    }
    .taskInfo .stateButton{
        width: 80px;
        margin-top: -10px;
    }
    .taskInfo p{
        font-size: 14px;
        word-break: break-all;
        margin: 5px 0px;
        color: #A9A9A9;
    }
    .taskInfo .footer{
        font-size: 14px;
        color: #A9A9A9;
    }
    .line{
        margin: 10px 0px;
        height: 1px;
        background-color: #A9A9A9;
    }
    .stateInfo{
        margin: 10px 0px;
        font-size: 14px;
    }
    .stateContent{
        background-color: #F5F5F5;
        margin-top: 6px;
        padding: 10px 10px 10px 30px;
        word-break: break-all;

    }
    .stateContent.feedback{
        border-left: 2px solid #5cb85c;
    }
    .stateContent.need{
        border-left: 2px solid #d9534f;
    }
    .stateContent.opinion{
        border-left: 2px solid #337ab7;
    }
    .fromCard{
        margin: 10px 0px;
        background-color: #ffffff;
        padding: 10px;
    }
    .fromCard .textArea{
        margin: 15px 1px;
        -moz-user-select:text !important;/*火狐*/
        -webkit-user-select:text !important;/*webkit浏览器*/
        -ms-user-select:text !important;/*IE10*/
        -khtml-user-select:text !important;/*早期浏览器*/
    }
    .fromCard button{
        width: 80%;
        display:block;
        margin:0 auto;
    }
    .stateSelect{
        margin: 15px 0px;
        color: #A9A9A9;
    }
</style>
<div class="content">
    <div class="taskInfo">
        <div id="taskInfoId"></div>
        <div class="line"></div>
        <span class="titleName">跟踪情况</span>
        <div id="feedBackInfoId"></div>
    </div>
    <div class="fromCard" id="userInfoId">
    </div>
    <input type="hidden" id="taskId" value="${taskId}"/>
    <input type="hidden" id="taskStatusId" value=""/>
</div>
<script>

    function convertDate(date) {
        var now = new Date(date);
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        return year + "" + (month < 10 ? "0" + month : month) + "" + (day < 10 ? "0" + day : day);
    }

    $(function(){
        var taskId = $("#taskId").val();
        if(taskId != null && taskId != "" && taskId != undefined) {
            //加载项目板块信息
            loadCategoryTaskList(taskId);
            //加载反馈信息
            loadFeedbackList(taskId);
            //加载用户角色
            loadUserRoleList(taskId);
        }
    });

    /**
     * 加载板块与工作列表
     * @param taskId
     */
    function loadCategoryTaskList(taskId) {
        $.post("findTaskDetailInfo", {"taskId": taskId}, function(taskVo){
            $("#taskInfoId").html(dealWithTaskInfo(taskVo));
        });
    }

    /**
     * 加载反馈信息
     * @param taskId
     */
    function loadFeedbackList(taskId) {
        $.post("findFeedBackInfo", {"taskId": taskId}, function(feedbackVOList){
            $("#feedBackInfoId").html(dealWithFeedBackInfo(feedbackVOList));
        });
    }

    /**
     * 加载用户角色信息
     * @param taskId
     */
    function loadUserRoleList(taskId) {
        $.post("findUserRoleInfo", {"taskId": taskId}, function(userInfo){
            $("#userInfoId").html(dealWithUserRoleInfo(userInfo));
        });
    }

    function dealWithUserRoleInfo(userInfo) {
        var divHtml = "";
        if(userInfo.role == '1') {
            divHtml += '<span class="titleName">董事长指示</span><br>'
                + '<textarea id="contentOneId" class="form-control textArea" rows="4" maxlength="1000" placeholder="最多只能输入1000个字"></textarea>'
                + '<button type="button" class="btn btn-primary" onclick="addFeedBack(' + userInfo.role + ',  \'' + userInfo.taskId + '\')">确认提交</button>';
        }
        if(userInfo.role == '2' || userInfo.role == '3') {
            divHtml += '<span class="titleName">意见</span><br>'
                + '<textarea id="contentTwoId" class="form-control textArea" rows="4" maxlength="1000" placeholder="最多只能输入1000个字"></textarea>'
                + '<button type="button" class="btn btn-primary" onclick="addFeedBack(' + userInfo.role + ',  \'' + userInfo.taskId + '\')">确认提交</button>';
        }
        if(userInfo.role == '4') {
            divHtml += '<span class="titleName">项目反馈</span><br><div class="stateSelect"><span>项目状态：</span>';
            divHtml += '<label class="radio-inline">';
            if(userInfo.task.status == 'D') {
                divHtml += '<input type="radio"  value="D" checked name="state" onclick="changeStatus(\'D\')"> 延期';
            } else {
                divHtml += '<input type="radio"  value="" name="state" onclick="changeStatus(\'D\')"> 延期';
            }
            divHtml += '</label>';

            divHtml += '<label class="radio-inline">';
            if(userInfo.task.status == 'G') {
                divHtml += '<input type="radio"  value="G" checked name="state" onclick="changeStatus(\'G\')"> 进行中';
            } else {
                divHtml += '<input type="radio"  value="" name="state" onclick="changeStatus(\'G\')"> 进行中';
            }
            divHtml += '</label>';

            divHtml += '<label class="radio-inline">';
            if(userInfo.task.status == 'P') {
                divHtml += '<input type="radio"  value="P" checked name="state" onclick="changeStatus(\'P\')"> 暂停';
            } else {
                divHtml += '<input type="radio"  value="" name="state" onclick="changeStatus(\'P\')"> 暂停';
            }
            divHtml += '</label>';

            divHtml += '<label class="radio-inline">';
            if(userInfo.task.status == 'F') {
                divHtml += '<input type="radio"  value="F" checked name="state" onclick="changeStatus(\'F\')"> 已完成';
            } else {
                divHtml += '<input type="radio"  value="" name="state" onclick="changeStatus(\'F\')"> 已完成';
            }
            divHtml += '</label>';

            divHtml += '</div>'
            + '<textarea id="contentFourId" class="form-control textArea" rows="4" maxlength="1000" placeholder="最多只能输入1000个字"></textarea>'
            + '<button type="button" class="btn btn-primary" onclick="addFeedBack(' + userInfo.role + ', \'' + userInfo.taskId + '\')">确认提交</button>';
        }
        return divHtml;
    }



    function dealWithFeedBackInfo(feedbackVOList) {
        for(i=3;i<$("#mask_div00").siblings().length;i++){
            $("#mask_div00").siblings()[i].remove();
            i--
        }
        $("#mask_div00").remove();
        var divHtml = "";
        for(var i in feedbackVOList) {
            divHtml += '<div class="stateInfo">';
            if(feedbackVOList[i].userTask.role == '1') {
                divHtml += '<button type="button" class="btn btn-danger btn-xs">董事长指示</button>';
            } else if(feedbackVOList[i].userTask.role == '2' || feedbackVOList[i].userTask.role == '3') {
                divHtml += '<button type="button" class="btn btn-primary btn-xs">意见</button>';
            } else if(feedbackVOList[i].userTask.role == '4') {
                divHtml += '<button type="button" class="btn btn-success btn-xs">反馈</button>';
            }
            divHtml += '<span>' + feedbackVOList[i].feedbackTime + '</span>';
//            if(feedbackVOList[i].userTask.role == '1') {
//                divHtml += '<span class="pull-right">提出人：' + feedbackVOList[i].user.userName + '</span>';
//            }else
            if(feedbackVOList[i].userTask.role == '3') {
                divHtml += '<span class="pull-right">集团领导：' + feedbackVOList[i].user.userName + '</span>';
            } else
                if(feedbackVOList[i].userTask.role == '4') {
                divHtml += '<span class="pull-right">目标责任人：' + feedbackVOList[i].user.userName + '</span>';
            } else if(feedbackVOList[i].userTask.role == '2') {
                divHtml += '<span class="pull-right">干系人：' + feedbackVOList[i].user.userName + '</span>';
            }
            divHtml += '<div class="stateContent feedback">' + feedbackVOList[i].feedbackContent + '</div>';
            divHtml += '</div>';
        }
        $.post("findUserName", null, function(user){
            var text = user.userName + " " + convertDate(new Date());
            $("#mask_div00").remove();
            $("#mask_div00").siblings().remove();
            setTimeout(watermarkCanvas({ watermark_txt: text }),1000);
        });
        return divHtml;
    }

    function dealWithTaskInfo(taskVo) {
        var divHtml = "";
        divHtml += '<div style="margin-bottom: 10px"><span class="titleName">' + taskVo.category.categoryName + '</span>';
        if(taskVo.status == 'D') {
            divHtml += '<button type="button" class="btn btn-danger btn-xs pull-right stateButton">延期</button>';
        } else if(taskVo.status == 'P') {
            divHtml += '<button type="button" class="btn btn-warning btn-xs pull-right stateButton">暂停</button>';
        } else if(taskVo.status == 'F') {
            divHtml += '<button type="button" class="btn btn-primary btn-xs pull-right stateButton">已完结</button>';
        } else if(taskVo.status == 'G'){
            divHtml += '<button type="button" class="btn btn-success btn-xs pull-right stateButton">进行中</button>';
        }
        divHtml += '<span style="word-break: break-all;font-size: 16px">　　' + taskVo.dept.deptName + '</span></div>';
        divHtml += '<span>' + taskVo.taskName + '</span>';
//        divHtml += '<p>提出人：';
//        for(var i in taskVo.userVoList) {
//            if(taskVo.userVoList[i].userTask.role == '1') {
//                divHtml += '&nbsp;' + taskVo.userVoList[i].userName + '&nbsp;';
//            }
//        }
//        divHtml += '</p>';


        divHtml += '<p>责任人：';
        for(var i in taskVo.userVoList) {
            if(taskVo.userVoList[i].userTask.role == '4') {
                divHtml += '&nbsp;' + taskVo.userVoList[i].userName + '&nbsp;';
            }
        }
        divHtml += '</p>';

        //如果有领导值，则显示该标签
//        if(judgeIsShow(taskVo.userVoList, 3)) {
//            divHtml += '<p>集团领导：';
//            for(var i in taskVo.userVoList) {
//                if(taskVo.userVoList[i].userTask.role == '3') {
//                    divHtml += '&nbsp;' + taskVo.userVoList[i].userName + '&nbsp;';
//                }
//            }
//            divHtml += '</p>';
//        }

        divHtml += '<div class="footer">';
//        if(judgeIsShow(taskVo.userVoList, 3)) {
//            divHtml += '<span>集团领导：';
//            for(var i in taskVo.userVoList) {
//                if(taskVo.userVoList[i].userTask.role == '3') {
//                    divHtml += '&nbsp;' + taskVo.userVoList[i].userName + '&nbsp;';
//                }
//            }
//            divHtml += '</span>';
//        }

        if(judgeIsShow(taskVo.userVoList, 2)) {
            divHtml += '<span>干系人：';
            for(var i in taskVo.userVoList) {
                if(taskVo.userVoList[i].userTask.role == '2') {
                    divHtml += '&nbsp;' + taskVo.userVoList[i].userName + '&nbsp;';
                }
            }
            divHtml += '</span>';
        }
        divHtml += '</div>';


        divHtml += '<div class="footer"><span>开始时间：' + (taskVo.startTime == null ? '' : taskVo.startTime) + '</span><span class="pull-right">计划完成时间：' + (taskVo.closeTime == null ? '' : taskVo.closeTime) + '</span></div>';
//        divHtml += '<p>开始时间：' + taskVo.startTime + '</p><p>计划完成时间：' + taskVo.closeTime + '</p>';
        divHtml += '<p style="color: #333333">' + taskVo.taskContent + '</p>';
        return divHtml;
    }

    /**
     * 判断是否显示某个标签
     * @param list
     * @param roleId
     * @returns {boolean}
     */
    function judgeIsShow(list, roleId) {
        var isShow = false;
        for(var i in list) {
            if(list[i].userTask.role == roleId) {
                isShow = true;
                break;
            }
        }
        return isShow;
    }

    function addFeedBack(roleId, taskId) {
        var feedbackContent = "";
        if(roleId == '1') {
            feedbackContent = $("#contentOneId").val().trim();
        } else if(roleId == '2' || roleId == '3') {
            feedbackContent = $("#contentTwoId").val().trim();
        } else if(roleId == '4') {
            feedbackContent = $("#contentFourId").val().trim();
        }
        if(feedbackContent == "" || feedbackContent == undefined) {
            alert("请输入内容");
            return;
        }
        var taskStatus = $("#taskStatusId").val();

        var params = {"roleId": roleId, "taskId": taskId, "taskStatus": taskStatus, "feedbackContent": feedbackContent};
        $.post("addFeedback", params, function(data){
            console.log(data)
            //加载项目板块信息
            loadCategoryTaskList(taskId);
            //加载反馈信息
            loadFeedbackList(taskId);
            //清除输入空
            clearTextContent(roleId);
        });
    }

    function changeStatus(status) {
        $("#taskStatusId").val(status);

    }

    function clearTextContent(roleId) {
        if(roleId == '1') {
            $("#contentOneId").val("");
        } else if(roleId == '2' || roleId == '3') {
            $("#contentTwoId").val("");
        } else if(roleId == '4') {
            $("#contentFourId").val("");
        }
    }

</script>
</body>
</html>