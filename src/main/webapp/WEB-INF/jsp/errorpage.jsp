<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
    <title>错误页面</title>
</head>
<body style="text-align: center; vertical-align: middle">
    <div style="font-size: 16px; font-weight: bold; color: red; height: 50px;">您暂时无法查看此页面，如有疑问请联系管理员</div>
</body>
</html>
