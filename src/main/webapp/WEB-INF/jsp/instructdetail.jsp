<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
    <title>指示详情</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/shuiyin.js"></script>
</head>
<body>
<style>
    * {
        padding: 0;
        margin: 0;
        -moz-user-select: none; /*火狐*/
        -webkit-user-select: none; /*webkit浏览器*/
        -ms-user-select: none; /*IE10*/
        -khtml-user-select: none; /*早期浏览器*/
        user-select: none;
        overflow-x: hidden;
    }

    .content {
        width: 100%;
        min-height: 100vh;
        overflow: hidden;
        background-color: #f0f2f9;
        padding: 10px;
    }

    .instructInfo {
        background-color: #ffffff;
        min-height: 100px;
        padding: 10px;
    }

    .titleName {
        color: #171b6d;
        font-size: 18px;
    }

    .line {
        margin: 10px 0px;
        height: 1px;
        background-color: #A9A9A9;
    }

    .stateInfo {
        margin: 10px 0px;
        font-size: 12px;
    }

    .stateContent {
        background-color: #F5F5F5;
        margin-top: 6px;
        padding: 10px 10px 10px 30px;
        word-break: break-all;

    }

    .stateContent.feedback {
        border-left: 2px solid #5cb85c;
    }

    .stateContent.need {
        border-left: 2px solid #d9534f;
    }

    .stateContent.opinion {
        border-left: 2px solid #337ab7;
    }

    .fromCard {
        margin: 10px 0px;
        background-color: #ffffff;
        padding: 10px;

    }

    .fromCard .textArea {
        margin: 15px 1px;
        -moz-user-select: text !important; /*火狐*/
        -webkit-user-select: text !important; /*webkit浏览器*/
        -ms-user-select: text !important; /*IE10*/
        -khtml-user-select: text !important; /*早期浏览器*/
    }

    .fromCard button {
        width: 80%;
        display: block;
        margin: 0 auto;
    }

    .aQuality label {
        width: 23%;
    }

    .aQuality p {
        word-break: break-all;
        width: 77%;
        font-size: 13px
    }
</style>
<div class="content">
    <div class="instructInfo">
        <div style="margin-bottom: 10px" id="instruct_area">
            <%--<div class="aQuality">--%>
            <%--<label class="pull-left" >指示时间：</label>--%>
            <%--<p class="pull-left">YYYY-MM-DD  HH:MM:SS</p>--%>
            <%--</div>--%>
            <%--<div class="aQuality">--%>
            <%--<label class="pull-left" >指示内容：</label>--%>
            <%--<p class="pull-left" >去我54654654654984798穷乌尔奇沃尔去的后UI去我的后UI去我的后去还万达公寓还过渡语IQ我跟丢五个地区蛋糕IQU盾贵气的购物IQ我的  </p>--%>
            <%--</div>--%>
            <%--<div class="aQuality">--%>
            <%--<label class="pull-left" >责 任 人：</label>--%>
            <%--<p class="pull-left" >张小三、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四、李小四</p>--%>
            <%--</div>--%>
        </div>
        <div class="line"></div>
        <span class="titleName">跟踪情况</span>
        <div id="track_list_area">
            <%--<div class="stateInfo">--%>
            <%--<button type="button"  class="btn btn-success btn-xs">反馈</button>--%>
            <%--<span>2019.02.05 15:12:30</span><span class="pull-right">目标责任人：张小三</span>--%>
            <%--<div class="stateContent feedback">--%>
            <%--你开始觉得他越来越无法满足你的全部幻想全部期待，--%>
            <%--对望的时候，彼此的眼里充满了空洞犹疑。杯子里面的水终于全部消失。--%>
            <%--342378462389493024890238409234820938409234792734982364--%>
            <%--</div>--%>
            <%--</div>--%>
            <%--<div class="stateInfo">--%>
            <%--<button type="button"  class="btn btn-danger btn-xs">指示</button>--%>
            <%--<span>2019.02.05 15:12:30</span><span class="pull-right">董事长</span>--%>
            <%--<div class="stateContent need">--%>
            <%--你开始觉得他越来越无法满足你的全部幻想全部期待，--%>
            <%--对望的时候，彼此的眼里充满了空洞犹疑。杯子里面的水终于全部消失。--%>
            <%--342378462389493024890238409234820938409234792734982364--%>
            <%--</div>--%>
            <%--</div>--%>
        </div>

    </div>
    <div class="fromCard" id="edit_area">
        <%--<span class="titleName">反馈</span><br>--%>
        <%--<textarea id="feedback_content_id" class="form-control textArea" rows="4"></textarea>--%>
        <%--<button type="button" class="btn btn-primary" onclick="addInstructFeedBack()">确认提交</button>--%>
    </div>
    <%--<div class="fromCard">--%>
        <%--<span class="titleName">董事长指示</span><br>--%>
        <%--<textarea id="instruct_content_id" class="form-control textArea" rows="4"></textarea>--%>
        <%--<button type="button" class="btn btn-primary" onclick="addInstructFeedBack()">确认提交</button>--%>
    <%--</div>--%>
    <input type="hidden" id="instruct_id" value="${instructId}"/>
</div>
<script>

    $(function () {
        var instructId = $("#instruct_id").val();
        if (instructId != '') {
            loadInstructInfo(instructId);
            loadInstructFeedbackInfo(instructId);
            loadCurrentUserRole();
        }
    });


    function loadInstructInfo(instructId) {
        $.post("findInstructById", {"instructId": instructId}, function (instructVO) {
            $("#instruct_area").html(dealWithInstructInfo(instructVO));
        });
    }

    function loadInstructFeedbackInfo(instructId) {
        $.post("findInstructFeedBack", {"instructId": instructId}, function (instructFeedbackVOList) {
            $("#track_list_area").html(dealWithInstructFeedbackInfo(instructFeedbackVOList));
        });
    }

    function loadCurrentUserRole() {
        $.post("findCurrentUserRole", null, function (user) {
            $("#edit_area").html(dealWithEditArea(user));
        });
    }

    function addInstructFeedBack(roleId) {
        var instructId = $("#instruct_id").val();
        var instructFeedbakcContent = '';
        if(roleId == 1) {
            instructFeedbakcContent = $("#instruct_content_id").val().trim();
        } else {
            instructFeedbakcContent = $("#feedback_content_id").val().trim();
        }
        if(instructFeedbakcContent == '') {
            alert("请输入内容");
            return;
        }
        var params = {"instructId": instructId, "roleId": roleId, "feedbackContent": instructFeedbakcContent};
        $.post("addInstructFeedback", params, function (ret) {
            loadInstructInfo(instructId);
            loadInstructFeedbackInfo(instructId);
            loadCurrentUserRole();
        });

    }

    function dealWithInstructInfo(instructVO) {
        var divHtml = '';
        divHtml += '<div class="aQuality"><label class="pull-left" >指示时间：</label>'
            + '<p class="pull-left">' + instructVO.time + '</p>'
            + '</div>';
        divHtml += '<div class="aQuality"><label class="pull-left" >指示内容：</label>'
            + '<p class="pull-left" >' + instructVO.instructContent + '</p>'
            + '</div>';
        divHtml += '<div class="aQuality"><label class="pull-left" >责 任 人：</label>';
        divHtml += '<p class="pull-left" >';
        for (var i in instructVO.userList) {
            divHtml += '&nbsp;' + instructVO.userList[i].userName + '&nbsp;';
        }
        divHtml += '</p></div>';
        return divHtml;
    }


    function dealWithInstructFeedbackInfo(instructFeedbackVOList) {
        for(i=3;i<$("#mask_div00").siblings().length;i++){
            $("#mask_div00").siblings()[i].remove();
            i--
        }
        $("#mask_div00").remove();
        var divHtml = '';
        for (var i in instructFeedbackVOList) {
            divHtml += '<div class="stateInfo">'
            if (instructFeedbackVOList[i].roleId == 1) {
                divHtml += '<button type="button"  class="btn btn-danger btn-xs">指示</button>';
            } else {
                divHtml += '<button type="button"  class="btn btn-success btn-xs">反馈</button>';
            }
            divHtml += '<span>' + instructFeedbackVOList[i].feedbackTime + '</span>';
            if (instructFeedbackVOList[i].roleId == 1) {
                divHtml += '<span class="pull-right">董事长</span>';
            } else {
                divHtml += '<span class="pull-right">目标责任人：' + instructFeedbackVOList[i].user.userName + '</span>';
            }
            divHtml += '<div class="stateContent feedback">' + instructFeedbackVOList[i].feedbackContent + '</div>';
            divHtml += '</div>';
        }
        $.post("findUserName", null, function(user){
            var text = user.userName + " " + convertDate(new Date());
            $("#mask_div00").remove();
            $("#mask_div00").siblings().remove();
            setTimeout(watermarkCanvas({ watermark_txt: text }),1000);
        });
        return divHtml;
    }


    function dealWithEditArea(user) {
        var divHtml = '';
        if(user.lv == 1) {
            divHtml += '<span class="titleName">董事长指示</span><br>';
            divHtml += '<textarea id="instruct_content_id" class="form-control textArea" rows="4"></textarea>';
        } else {
            divHtml += '<span class="titleName">反馈</span><br>';
            divHtml += '<textarea id="feedback_content_id" class="form-control textArea" rows="4"></textarea>';
        }
        divHtml += '<button type="button" class="btn btn-primary" onclick="addInstructFeedBack(\'' + user.lv + '\')">确认提交</button>';
        return divHtml;
    }

    function convertDate(date) {
        var now = new Date(date);
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        return year + "" + (month < 10 ? "0" + month : month) + "" + (day < 10 ? "0" + day : day);
    }

</script>

</body>
</html>
